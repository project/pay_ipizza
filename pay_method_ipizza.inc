<?php

/**
 * @file
 */

class pay_method_ipizza extends pay_method {
  var $banklink_url = '';
  var $merchant_account = '';
  var $merchant_name = '';
  var $language = '';
  var $bank_public_key = '';
  var $merchant_private_key = '';
  var $merchant_private_key_pwd = '';
  var $merchant_password = '';
  var $merchant_id = '';
  var $payment_type = 'ipizza';
  var $service = '';
  var $char_encoding = '';
  var $byte_count = '';

  function available_currencies() {
    return array('EUR');
  }
  /**
  * This method will be called when the user needs to be sent to the gateway
  * provider for processing.  Override it to include valid request values.
  */
  function ipizza_request() {
    return 'ipizza/' .  $this->activity->paid . '/gobank';
  }
  /**
  * This method will be called on a return response (e.g. PayPal IPN)
  * including an unfiltered version of the $_REQUEST array.  This method must
  * be extended in order for your ipizza method to work!
  */
  function ipizza_response($response = array()) {
  }
  /**
  * Augment the available payment actions for transactions that include
  * Ipizza payment methods.
  *
  * The 'response' action will allow us to respond to updates from the gateway.
  */
  function set_valid_actions($pay_form, &$actions) {
    $actions['response'] = array(
      'title' => 'Response',
      'callback' => 'response_action',
      'valid states' => array('pending'),
    );
  }

  /**
  * Create a stub method to ensure that do_action('authorize') is executed.
  * If your subclass wants to change, store, or fixup data before it's sent to
  * the gateway, do it by overriding this method.
  *
  * The 'real' work will be done in the form_submit() function.
  */
  function authorize_action($values = array()) {
    $this->set_ipizza_pending($values);
  }

  /**
  * Create a stub method to ensure that do_action('complete') is executed.
  * If your subclass wants to change, store, or fixup data before it's sent to
  * the gateway, do it by overriding this method.
  *
  * The 'real' work will be done in the form_submit() function.
  */
  function complete_action($values = array()) {
    $this->set_ipizza_pending($values);

    // Look for an 'authorize' action in this activity's history.
    foreach ($this->activity->history() as $previous) {
      // If there was a successful authorization, copy its details.
      if ($previous->action == 'authorize' && $previous->result) {
        $this->activity->identifier = $previous->identifier;
        $this->activity->data = $previous->data;
        $this->activity->total = $previous->total;
        $this->payment_type = $previous->payment_type;
        $this->activity->action = 'complete';
      }
    }
  }
  /**
  * Implement a 'response' action, which interprets the response from a
  * Ipizza payment gateway.
  */
  function response_action($values = array()) {
    // Copy the submitted/stored values from the previous activity.
    foreach ($this->activity->history() as $previous) {
      if (in_array($previous->action, array('pending', 'authorize')) && $previous->result) {
        $this->__construct($previous->data);
        if ($previous->action == 'pending') {
          $this->activity->action = $previous->data['action'];
        }
      }
    }

    // Get a boolean response from the response parser.
    return $this->ipizza_response($values) ? 'complete' : 'canceled';
  }

  /**
  * Store values submitted by a form function, etc.  This will allow us to
  * construct a request to the gateway processor when necessary.
  */
  function set_ipizza_pending($values = array()) {
    $this->activity->data = $values;
    $this->activity->result = TRUE;

    // Store the current activity for response handling, but store 'pending'.
    $this->activity->data['action'] = $this->activity->action;
    $this->activity->action = 'pending';

    // The Ipizza handler may hardcode its payment_type.
    if (isset($this->payment_type)) {
      $this->activity->payment_type = $this->payment_type;
    }
  }
  function settings_form(&$form, &$form_state) {
    parent::settings_form($form, $form_state);
    $group = $this->handler();

    $form[$group]['ipizza']['#type'] = 'fieldset';
    $form[$group]['ipizza']['#collapsible'] = FALSE;
    $form[$group]['ipizza']['#title'] = t('Ipizza settings');
    $form[$group]['ipizza']['#group'] = $group;
    $form[$group]['ipizza']['banklink_url'] = array(
      '#type' => 'textfield',
      '#title' => t('Banklink url'),
      '#default_value' => $this->banklink_url,
      '#required' => TRUE,
      '#parents' => array($group, 'banklink_url'),
    );
     $form[$group]['ipizza']['service'] = array(
      '#type' => 'select',
      '#title' => t('Ipizza service'),
      '#options' => array(1001 => 1001, 1011 => 1011),
      '#default_value' => $this->service,
      '#required' => TRUE,
      '#parents' => array($group, 'service'),
    );
    $form[$group]['ipizza']['char_encoding'] = array(
      '#type' => 'select',
      '#title' => t('Ipizza character encoding attribute'),
      '#options' => array( 'VK_ENCODING' => 'VK_ENCODING', 'VK_CHARSET' => 'VK_CHARSET'),
      '#default_value' => $this->char_encoding,
      '#required' => TRUE,
      '#parents' => array($group, 'char_encoding'),
    );
    $form[$group]['ipizza']['byte_count'] = array(
      '#type' => 'checkbox',
      '#title' => t('Use byte count (Default is character count) THis is used at VK_MAC calculation'),
      '#default_value' => $this->byte_count,
      '#parents' => array($group, 'byte_count'),
    );
    $form[$group]['ipizza']['merchant_id'] = array(
      '#type' => 'textfield',
      '#title' => t('Merchant id'),
      '#default_value' => $this->merchant_id,
      '#required' => TRUE,
      '#parents' => array($group, 'merchant_id'),
    );
    $form[$group]['ipizza']['merchant_name'] = array(
      '#type' => 'textfield',
      '#title' => t('Merchant name'),
      '#default_value' => $this->merchant_name,
      '#required' => TRUE,
      '#parents' => array($group, 'merchant_name'),
    );
    $form[$group]['ipizza']['language'] = array(
      '#type' => 'textfield',
      '#title' => t('Bank response language'),
      '#description' => 'Use uppercase language code (EST, LAT, ENG, RUS)
         if its supported by your bank. If not sure just leave it empty',
      '#default_value' => $this->language,
      '#parents' => array($group, 'language'),
    );
    $form[$group]['ipizza']['merchant_account'] = array(
      '#type' => 'textfield',
      '#title' => t('Merchant account'),
      '#default_value' => $this->merchant_account,
      '#parents' => array($group, 'merchant_account'),
    );
    $form[$group]['ipizza']['bank_public_key'] = array(
      '#type' => 'textarea',
      '#title' => t('Bank public key'),
      '#default_value' => $this->bank_public_key,
      '#required' => TRUE,
      '#parents' => array($group, 'bank_public_key'),
    );
    $form[$group]['ipizza']['merchant_private_key'] = array(
      '#type' => 'textarea',
      '#title' => t('Merchant private key'),
      '#default_value' => $this->merchant_private_key,
      '#required' => TRUE,
      '#parents' => array($group, 'merchant_private_key'),
    );
    $form[$group]['ipizza']['merchant_private_key_pwd'] = array(
      '#type' => 'textfield',
      '#title' => t('Merchant private key password'),
      '#default_value' => $this->merchant_private_key_pwd,
      '#parents' => array($group, 'merchant_private_key_pwd'),
    );
  }


  function form(&$form, &$form_state) {
    parent::form($form, $form_state);

    if (isset($this->gateway_testmode) && $this->gateway_testmode) {
      drupal_set_message(t('The @name payment method is in test mode. This transaction will not be fully processed.', array('@name' => $this->title())), 'warning');
    }
    $method_form = array();
    //$form['#attached']['css'][] = drupal_get_path('module', 'pay_ipizza') . '/pay_ipizza.css';
    // Add this method_form to the expected place on the parent form.
    $form[$this->pay_form->handler()]['pay_method'][$this->pmid] = $method_form;
  }
  function form_submit(&$form, &$form_state) {
    // Find our transaction object provided by pay_form::form_submit().
    if (isset($form_state['pay_activity']) && $form_state['pay_activity']) {
      foreach ($form_state['pay_activity'] as $key => $activity) {
        if ($activity->pmid == $this->pmid) {
          // Load the activity as the 'current' one.
          $this->activity = pay_activity_load($activity->paid);
          $this->__construct($this->activity->data);
          $this->activity->action = $this->pay_form_action;

          // Effect a redirect to the direct payment gateway.
          if ($this->activity->action && $this->activity->action != 'pending') {
            unset($form_state['storage']);
            $form_state['rebuild'] = FALSE;
            $form_state['redirect'] = $this->ipizza_request();
            break;
          }
        }
      }
    }
  }
}
